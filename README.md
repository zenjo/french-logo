# French logo

## Résumé

Cette classe retourne un substitut de primitives logo en français, basées sur le module turtle

Elle comporte également une primitive repete récursive (absente du module turtle)

- Voir le [site du projet](https://framagit.org/zenjo/french-logo)
- Voir le [wiki du site du projet](https://framagit.org/zenjo/french-logo/wikis/home)
- Voir la [documentaton Sphinx du projet](https://docs.sebille.name/french-logo/)
- [French logo sur pypi](https://pypi.org/project/french-logo/).org
